import os
import re
from BaseHandler import BaseHandler

class TargetTestHandler(BaseHandler):
	
	def __init__(self):
		super(self.__class__,self).__init__()

	def collectResults(self,directory):
		
		subdir = os.path.join(directory, 'TargetOutput/ROOTGraphs')
		for file_ in os.listdir(subdir):
		    
		    filename = os.path.join(directory,subdir,file_)
		
		    self.saveFile(file_,filename)
