import os
import re
import json
from BaseHandler import BaseHandler


class RadLengthHandler(BaseHandler):
	
	def checkFile(self,file_):
		
	    if not os.path.exists(file_):
		raise Exception("File %s does not exist" % file_) 
		return False
	    else:
		return True

	def __init__(self):
		super(self.__class__,self).__init__()


	def collectResults(self,directory):
	
		subdir = 'Rad_length/root_files/'
		radroot = ['Rad_merged.root','Rad_velo_z.root','radRadPlots.root','interRadPlots.root']
 
		filenames = [os.path.join(directory,subdir,i) for i in radroot] 

		for i, filename in enumerate(filenames):
			if self.checkFile(filename):
				self.saveFile(radroot[i],filename)	
		
		p2pRadLengthfile = os.path.join(directory,'Rad_length/data_tables/p2p_radlength.json')
		p2pInterLengthfile = os.path.join(directory,'Rad_length/data_tables/p2p_interlength.json')	

		cumulzRadLengthfile = os.path.join(directory,'Rad_length/data_tables/cumulz_radlength.json')
		cumulzInterLengthfile = os.path.join(directory,'Rad_length/data_tables/cumulz_interlength.json')	
		cumulRadLengthfile = os.path.join(directory,'Rad_length/data_tables/cumul_radlength.json')
		cumulInterLengthfile = os.path.join(directory,'Rad_length/data_tables/cumul_interlength.json')	
		self.checkFile(p2pRadLengthfile)
		self.checkFile(p2pInterLengthfile)
		self.checkFile(cumulzInterLengthfile)
		self.checkFile(cumulzRadLengthfile)
		self.checkFile(cumulInterLengthfile)
		self.checkFile(cumulRadLengthfile)

		self.saveString("RADLENGTH_TABLE_p2p_RadLength",json.load(open(p2pRadLengthfile)))
		self.saveString("RADLENGTH_TABLE_p2p_InterLength",json.load(open(p2pInterLengthfile)))

		self.saveString("RADLENGTH_TABLE_cumulz_RadLength",json.load(open(cumulzRadLengthfile)))
		self.saveString("RADLENGTH_TABLE_cumulz_InterLength",json.load(open(cumulzInterLengthfile)))

		self.saveString("RADLENGTH_TABLE_cumul_RadLength",json.load(open(cumulRadLengthfile)))
		self.saveString("RADLENGTH_TABLE_cumul_InterLength",json.load(open(cumulInterLengthfile)))
